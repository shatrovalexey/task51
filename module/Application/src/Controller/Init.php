<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\ResultInterface;
use Zend\Db\ResultSet\ResultSet;

final class Init extends AbstractActionController
{
	/**
	* @var array $connection - параметры подключения к СУБД
	* @var Zend\Db\Adapter\Adapter $dbh - подключение к СУБД
	* @var array $results - возможные значения `test`.`result`
	*/

	private $connection = [
		'driver' => 'Pdo_Mysql' ,
		'database' => 'task51' ,
		'username' => 'root' ,
		'password' => 'f2ox9erm'
	] ;
	private $dbh ;
	private $results = [
		'normal' , 'illegal' , 'failed' , 'success'
	] ;

	/**
	* Конструктор
	*/
	public function __construct( ) {
		$this->dbh = new Adapter( $this->connection ) ;
	}

	/**
	* создает таблицу test, содержащую 5 полей:
	* id - целое, автоинкрементарное
	* script_name - строковое, длиной 25 символов
	* start_time - целое
	* end_time - целое
	* result - один вариант из 'normal', 'illegal', 'failed', 'success'
	*/
	private function create( ) {
		$this->dbh->query( "
CREATE TABLE `test` (
	`id` bigint( 22 ) unsigned NOT NULL AUTO_INCREMENT ,
	`script_name` varchar( 25 ) NOT NULL ,
	`start_time` bigint( 22 ) unsigned NOT NULL ,
	`end_time` bigint( 22 ) unsigned NOT NULL ,
	`result` enum( '" . implode( "','" , $this->results ) . "' ) NOT NULL DEFAULT '" . $this->results[ 0 ] . "' ,
	PRIMARY KEY ( `id` ) ,
	INDEX( `result` )
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;
		" , Adapter::QUERY_MODE_EXECUTE ) ;
	}

	/**
	* заполняет таблицу случайными данными
	*/
	private function fill( ) {
		for ( $i = 0 ; $i < 1e2 ; $i ++ ) {
			$this->dbh->query( '
INSERT INTO `test`
SET
	`script_name` := ? ,
	`start_time` := ? ,
	`end_time` := ? ,
	`result` := ? ;
			' , [
				uniqid( ) ,
				rand( 1 , 1e6 ) ,
				rand( 1e6 , 1e7 ) ,
				array_rand( $this->results )
			] )->execute( ) ;
		}
	}

	/**
	* выбирает из таблицы test, данные по критерию: result среди значений 'normal' и 'success'
	* @returns array - набор записей из `test`
	*/
	public function get( ) {
		$sth = $this->dbh->createStatement( "
SELECT
	`t1`.*
FROM
	`test` AS `t1`
WHERE
	( `t1`.`result` IN ( 'normal' , 'success' ) ) ;
		" ) ;

		$sth->prepare( ) ;
		$rh = $sth->execute( ) ;

		try {
			$rsh = new ResultSet( ) ;
			$rsh->initialize( $rh ) ;

			return $rsh ;
		} catch ( Exception $exception ) {
		}

		return [ ] ;
	}
}
